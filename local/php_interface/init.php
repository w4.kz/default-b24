<?php

//&noinit=yes
if (isset($_GET['noinit']) && $_GET['noinit'] === 'yes') {
    $_SESSION['work_without_init'] = 1;
}
if ( ! isset($_SESSION['work_without_init'])) {
	//загрузчик классов
	if (is_file($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/autoload.php')) 
	{
		require_once( $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/autoload.php');
	}
	// подключение обработчиков событий
    if (is_file($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/events.php')) 
	{
        require_once $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/events.php';
    }
	/*if (is_file($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/axnusershandlers.php')) 
	{
		require_once( $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/axnusershandlers.php');
	}	*/
}
?>