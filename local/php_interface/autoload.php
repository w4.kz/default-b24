<?
Bitrix\Main\Loader::registerAutoLoadClasses(null, [
    // ключ - имя класса с простанством имен, значение - путь относительно корня сайта к файлу
    'Local\Lib\axnUsersHandlers' => '/local/php_interface/lib/axnUsersHandlers.php',
	'Local\Lib\axnPpm' => '/local/php_interface/lib/axnPpm.php',
	'Local\Lib\axnPPMCtrlButton' => '/local/php_interface/lib/axnPPMCtrlButton.php'
]);