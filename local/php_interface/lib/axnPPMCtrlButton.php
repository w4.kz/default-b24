<?
//*********************************************************************************************************************
//* данный программный код является интеллектуальной собственностью ТОО "AXION.KZ", mail@axion.kz (с) 2023 
//* любое несогласованное копирование и изменение данного кода является нарушением законодательства об авторском праве
//*********************************************************************************************************************
/*
Local\Lib\axnPPMCtrlButton - кнопки (дочерние элементы, группа, отчетность, проекты)
*/
namespace Local\Lib;

use Bitrix\Main\UserField\TypeBase;

class axnPPMCtrlButton extends TypeBase {
	const USER_TYPE_ID = 'axn_ppm_link_button';      
	//метод должен вернуть ассоциативный массив с человекопонятным именем типа поля (DESCRIPTION) и базовым типом (BASE_TYPE);
	public static function GetUserTypeDescription ()
	{
		// \Bitrix\Main\Diag\Debug::writeToFile("GetUserTypeDescription", $varName = "arFields", $fileName = "/logText.log");
		return array(
			'USER_TYPE_ID' => static::USER_TYPE_ID,
			'CLASS_NAME' => __CLASS__,
			'DESCRIPTION' => 'axnPPMCtrlButton',
			'BASE_TYPE' => \CUserTypeManager::BASE_TYPE_STRING,
			'EDIT_CALLBACK' => array (__CLASS__,'GetPublicEdit'),
			'VIEW_CALLBACK' => array (__CLASS__,'GetPublicView'),
			'PrepareSettings' => array (__CLASS__, 'PrepareSettings')
        );
    }
      
    //метод возвращает MySQL тип колонки, в которой будет храниться значение одиночного поля. Множественные поля всегда хранятся как text.
    public static function GetDBColumnType ($arUserField)
    {
        global $DB;
        //\Bitrix\Main\Diag\Debug::writeToFile("GetDBColumnType", $varName = "arFields", $fileName = "/logText.log");
        switch (strtolower($DB->type))
        {
            case "mysql":
            return "text";
            case "oracle":
            return "varchar2(2000 char)";
            case "mssql":
            return "varchar(2000)";
        }
    }
	
	public static function PrepareSettings($arUserField) {
		// DEFAULT VALUE
		return array(
			'DEFAULT_VALUE' => '1'
		);
	}
	
    public static function GetPublicView ($arUserField , $arAdditionalParameters = array())
    {
        \Bitrix\Main\UI\Extension::load('ui.buttons');
		\CModule::IncludeModule('socialnetwork');
		\CModule::IncludeModule('crm');
		
		$entityUfID = $arUserField['ENTITY_ID']; //CRM_XX
		
		$entityTypeID =  \CCrmOwnerType::ResolveIDByUFEntityID($entityUfID); //NNN
		$itemID = $arUserField['ENTITY_VALUE_ID'];		
		
		//child items button
		$childEntityTypeID =  $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($arUserField['ENTITY_ID'], $arUserField['ENTITY_VALUE_ID'])['UF_'.$entityUfID.'_GENERAL_CHILD_ENTITY_ID']['VALUE'];
		if (\CCrmOwnerType::IsDefined($childEntityTypeID)) {
			$childEntityName = \CCrmOwnerType::GetDescription($childEntityTypeID);
			$childItemsUrl = '/ppm/childitems.php';
			$childItemsUrlParams = '?parent='.$entityTypeID.'&entity='.$childEntityTypeID.'&item='.$itemID;	
			//get count
			$factoryChildItems = \Bitrix\Crm\Service\Container::getInstance()->getFactory($childEntityTypeID);
			$parentField = 'PARENT_ID_'.$entityTypeID; 
			$arFilter = [$parentField => $itemID];
			$childItems = $factoryChildItems->getItems([
				'select' => ['ID'],
				'filter' => $arFilter 
			]);
			$countChildItems = count($childItems);
			$countChildItemsDiv = '&nbsp<div class="ui-counter ui-counter-sm ui-counter-light"><div class="ui-counter-inner">'.$countChildItems.'</div></div>';
			
			//create html
			//TODO: вынести скрипт в prolog!!!! с передачей параметров
			$script_child = '<script>$(".ppm_show_child_items_'.$itemID.'").click(function(){BX.SidePanel.Instance.open("'.$childItemsUrl.$childItemsUrlParams.'", {requestMethod: "post",width:2000,customLeftBoundary:0,cacheable:true,mobileFriendly:true,allowChangeHistory:false, label:{text:"",color:"#FFFFFF",bgColor:"#E2AE00",opacity:80},});});</script>';	
			
			$span_child = '<button title="Дочерние элементы: '.$childEntityName.'" class="ppm_show_child_items_'.$itemID.' ui-btn ui-btn-xs ui-btn-light-border">'.'<img src="/ppm/img/portfolio.svg" alt="проектный офис" width="14" height="14">'.$countChildItemsDiv.'</button>'; //💼
			
			/*$span_child = '<a title="Дочерние элементы: '.$childEntityName.'" href="'.$childItemsUrl.$childItemsUrlParams.'" class="ppm_show_child_items'.$itemID.' ui-btn ui-btn-xs ui-btn-light-border">'.'<img src="/ppm/img/portfolio.svg" alt="проектный офис" width="14" height="14">'.$countChildItemsDiv.'</a>'; */
			
		}
		
		\CJSCore::Init(array("jquery"));
        return '<span style="white-space: nowrap;">'.$span_child.$span_projects.'</span>'.$script_child;
    }

    public static function GetPublicEdit ($arUserField , $arAdditionalParameters = array())
    {
        $name = static::getFieldName($arUserField, $arAdditionalParameters);
        return self::GetPublicView($arUserField , $arAdditionalParameters).'<input type="hidden" name="'. $name .'" value="1"  />'; //type="hidden" 
	}
      

//CLASS END
}