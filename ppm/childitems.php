<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->RestartBuffer();

\CModule::IncludeModule('crm');
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$entityTypeId = $request["entity"];
$parentEntityTypeId = $request["parent"];
$parentEntityId = $request["item"];
$entityName = \CCrmOwnerType::GetDescription($entityTypeId);
$parentName = \CCrmOwnerType::GetDescription($parentEntityTypeId);

$factory = \Bitrix\Crm\Service\Container::getInstance()->getFactory($parentEntityTypeId);
$parentItem = $factory->getItem($parentEntityId);		
$parentTitle = $parentItem['TITLE'];
$APPLICATION->SetTitle($parentName.': '.$parentTitle);
$APPLICATION->SetPageProperty('title', $parentName.': '.$parentTitle);
?>

<?

/*\Bitrix\UI\Toolbar\Facade\Toolbar::deleteFavoriteStar();
$titleHtmlContent = '<span class="ui-toolbar-title-item">'.$parentEntityTypeId.$parentName.': '.$parentTitle.' -></span>';
$toolbar = \Bitrix\UI\Toolbar\Manager::getInstance()->getToolbarById(\Bitrix\UI\Toolbar\Facade\Toolbar::DEFAULT_ID);
$toolbar->addBeforeTitleHtml($ourHtmlContent);
*/

?>
<!DOCTYPE html>
<html>
<head>
	<?$APPLICATION->ShowHead(); $APPLICATION->ShowHeadStrings();?>
	<h1><? $APPLICATION->ShowTitle();?></h1>
</head>
<body>



<?//Контент компонентов

$parentTitle = $parentName.': '.$parentTitle;
$componentName = "w4a:crm.item.list";
$arParams = Array(
	"entityTypeId" =>$entityTypeId,
	"parentEntityTypeId" => $parentEntityTypeId,
	"parentEntityId" => $parentEntityId,
    "parentTitle" => $parentTitle,
);
if (isset($_REQUEST["IFRAME"]) && $_REQUEST["IFRAME"] === "Y") {
	$APPLICATION->IncludeComponent(
		'bitrix:ui.sidepanel.wrapper',
		'',
		[
			'POPUP_COMPONENT_USE_BITRIX24_THEME' => 'Y',
			//'THEME_ID' => 'light:tulips',
			//'DEFAULT_THEME_ID' => 'light:mail',
			'POPUP_COMPONENT_NAME' => $componentName,
			'POPUP_COMPONENT_TEMPLATE_NAME' => '',
			'POPUP_COMPONENT_PARAMS' => $arParams,
			'USE_UI_TOOLBAR' => 'Y',
			'USE_PADDING' => false,
			'PLAIN_VIEW' => false,
			'PAGE_MODE' => false,
			'PAGE_MODE_OFF_BACK_URL' => "/page/pmo/portfel/",
			'RELOAD_GRID_AFTER_SAVE' => true

		]
	);
} else {
	$APPLICATION->ShowHead();
	$APPLICATION->IncludeComponent(
		$componentName,
		'',
		$arParams
	);
}
?>

</body>
</html>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
//\CMain::FinalActions();
?>

 




